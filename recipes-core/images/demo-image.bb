DESCRIPTION = "Demo image for Mirametrix"
LICENSE = "CLOSED"

require recipes-core/images/core-image-minimal.bb

IMAGE_INSTALL_append_m3ulcb = " \
        kernel-image \
        kernel-devicetree \
 "

IMAGE_INSTALL += " \
        mase-yocto-demo \
        mase-prebuilt-tools \
"

# 8 GB extra 
# base unit is KB => 8 * 1024 * 1024
IMAGE_ROOTFS_EXTRA_SPACE = "8388608"
